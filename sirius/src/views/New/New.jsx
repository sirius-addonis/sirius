import React, { useContext, useEffect, useState } from "react";
import LinksToAddons from "../../components/LinksToAddons/LinksToAddons";
import PaginatedItems from "../../components/PaginatedItems/PaginatedItems";
import SearchBar from "../../components/SearchBar/SearchBar";
import AppContext from "../../providers/AppContext";
import { getAllAddons } from "../../services/addons.service";

const New = () => {
  const { setContext, ...appState } = useContext(AppContext);
  const [addons, setAddons] = useState([]);

  useEffect(() => {
    getAllAddons().then(setAddons).catch(console.error);
  }, []);
  const test = addons
    .sort(function (a, b) {
      return b.createdOn - a.createdOn;
    })
    .filter((e) => e.content.verified === true);

  return (
    <div>
      <LinksToAddons />
      <SearchBar />
      <PaginatedItems itemsPerPage={5} addons={test} />
    </div>
  );
};

export default New;
