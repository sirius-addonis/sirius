import React, { useContext, useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'
import Download from '../../components/Download/Download'
import MyRating from '../../components/MyRating/MyRating'
import AppContext from '../../providers/AppContext'
import { getAllAddons } from '../../services/addons.service'
import './MyAddons.css'

const MyAddons = () => {

  const { setContext, ...appState } = useContext(AppContext);
  const { userData }= useContext(AppContext)
  const [addons, setAddons] = useState([])

  useEffect (()=> {
    getAllAddons()
    .then((e)=> e.filter(el=> el.author === userData.handle))
    .then(setAddons)
    .catch(console.error)
  }, [])
  
  return (
    <div>
  
       <div className='MyAddons'>
         
         {addons.length === 0
         ? <p>No addons to show.</p>
          : 
          
          
          
          addons.map(addon =>  {
          return <div key={addon?.content?.imageURL} className={appState.theme === 'LightTheme' ? "SingleViewAddonContainer" : "SingleViewAddonContainerDark"} >
            
            <NavLink to={`../addons/all-addons/${addon.id}` } className='ViewDetailsLinkAllAddon' >
              <div className='UpperPartGoToViewDetails'>
                <div  className='SingleViewAddon'>
                  <h6>{addon?.content?.name}</h6>
                  <div className='AddonInfoUp'>
                    <MyRating id={addon.id}></MyRating>
                    <img src={addon?.content?.imageURL} alt='addon' className='SingleImageAddon'></img>
                    <p className='IDE'>IDE: {addon?.content?.IDE}</p>
                  </div>
                </div>
              </div>
            </NavLink>


            <div className='DownPartGoToDownloads'>
              <div className='DownloadContainer' >
                <Download id={addon.id} />
              </div>
            </div>

          </div> })

          
         }

       </div>

    </div>
  )
}

export default MyAddons