import "./ViewDetails.css";
import React, { useContext, useEffect, useState } from "react";
import Planets0 from "../../components/Planets/Planets0";
import Planets1 from "../../components/Planets/Planets1";
import Planets2 from "../../components/Planets/Planets2";
import Planets3 from "../../components/Planets/Planets3";
import Planets4 from "../../components/Planets/Planets4";
import Planets5 from "../../components/Planets/Planets5";
import like from "./../../../src/images/like.png";
import likePressed from "./../../../src/images/like-pressed.png";
import dislike from "./../../../src/images/dislike.png";
import dislikePressed from "./../../../src/images/dislike-pressed.png";
import love from "./../../../src/images/love.png";
import lovePressed from "./../../../src/images/love-pressed.png";
import Button from "react-bootstrap/Button";
//import Button from '@mui/material/Button';
import { useNavigate, useParams } from "react-router-dom";
import {
  getAddonById,
  likeAddons,
  removeLikeAddon,
  dislikeAddons,
  deleteAddon,
  getFavoriteStatus,
  setRatingDataFunction,
  getRating,
  checkIsLikedByHandle,
  checkIsDislikedByHandle,
  getAllLiked,
  getAllDisliked,
  removeDislikeAddon,
  deleteAddonFile,
  deleteAddonImage,
  verify,
} from "../../services/addons.service";
import {
  addUserFavorites,
  removeUserFavorites,
} from "../../services/users.service";
import AppContext from "../../providers/AppContext";
import {
  addComment,
  commentAddons,
  getAllComments,
} from "../../services/comments.service";
import { v4 } from "uuid";
import CreateComment from "../../components/CreateComment/CreateComment";
import AllComments from "../../components/AllComments/AllComments";
import MyRating from "../../components/MyRating/MyRating";
import OldVersions from "../../components/OldVersions/OldVersions";
import Download from "../../components/Download/Download";
import Featured from "../../components/Featured/Featured";

const ViewDetails = () => {
  const navigate = useNavigate();

  const { id } = useParams();
  const {
    userData: { handle, profilePicture },
  } = useContext(AppContext);

  const [rating, setRating] = useState([0]);
  const [rated, setRated] = useState(true);
  const [ratingData, setRatingData] = useState();
  const [fav, setFav] = useState();
  const [addon, setAddon] = useState({});
  const { userData } = useContext(AppContext);
  const [idOfComment, setIdOfComment] = useState("");
  const [done, setDone] = useState("");
  const [emptyComment, setEmptyComment] = useState("");
  const [comment, setComment] = useState([]);
  const [comments, setComments] = useState([]);
  //const [value, setValue] = useState(1);
  const [likes, setLikes] = useState([0]);
  const [dislikes, setDislikes] = useState([0]);
  const [liked, setLiked] = useState();
  const [disliked, setDisliked] = useState();

  const result = {
    textComment: comment,
    addonId: addon.id,
    likedBy: [],
  };

  // ---------- likes & Dislikes -----------------

  useEffect(() => {
    checkIsLikedByHandle(id, handle).then((result) => {
      setLiked(result);
    });
  }, [liked, id, handle]);

  // ---------- likes & Dislikes -----------------
  useEffect(() => {
    checkIsLikedByHandle(id, handle).then((result) => {
      setLiked(result);
    });
  }, [liked, id, handle]);

  useEffect(() => {
    checkIsDislikedByHandle(id, handle).then((result) => {
      setDisliked(result);
    });
  }, [disliked, id, handle]);

  useEffect(() => {
    getAllLiked(id).then(setLikes).catch(console.err);
  }, [liked, id]);

  useEffect(() => {
    getAllDisliked(id).then(setDislikes).catch(console.err);
  }, [disliked, id]);

  useEffect(() => {
    if (isNaN(likes.length)) {
      setLikes([]);
    }
    if (isNaN(dislikes.length)) {
      setDislikes([]);
    }
    if (likes.length === 0 && dislikes.length === 0) {
      setRatingData(0);
      setRatingDataFunction(id, 0);
    } else {
      let rating = (likes.length * 100) / (likes.length + dislikes.length);
      setRatingData(rating);
      setRatingDataFunction(id, rating);
    }
  }, [likes, dislikes, id]);

  useEffect(() => {
    if (isNaN(likes.length)) {
      setLikes([]);
    }
    if (isNaN(dislikes.length)) {
      setDislikes([]);
    }
    if (likes.length === 0 && dislikes.length === 0) {
      setRatingData(0);
      setRatingDataFunction(id, 0);
    } else {
      let rating = (likes.length * 100) / (likes.length + dislikes.length);
      setRatingData(rating);
      setRatingDataFunction(id, rating);
    }
  }, [likes, dislikes, id]);

  const AddLikeAddon = () => {
    if (liked === true) {
      removeLikeAddon(handle, id);
      setLiked(false);

      dislikeAddons(userData.handle, id);
      setDisliked(true);
    } else {
      likeAddons(handle, id);
      setLiked(true);
      removeDislikeAddon(userData.handle, id);
      setDisliked(false);
    }
  };

  const AddDislikeAddon = () => {
    if (disliked === true) {
      removeDislikeAddon(userData.handle, id);
      setDisliked(false);

      likeAddons(handle, id);
      setLiked(true);
    } else {
      dislikeAddons(userData.handle, id);
      setDisliked(true);

      removeLikeAddon(handle, id);
      setLiked(false);
    }
  };

  const FillPlanetsBar = () => {
    if (ratingData === 0) {
      return <Planets0 />;
    } else if (ratingData < 20) {
      return <Planets1 />;
    } else if (ratingData < 40) {
      return <Planets2 />;
    } else if (ratingData < 60) {
      return <Planets3 />;
    } else if (ratingData < 80) {
      return <Planets4 />;
    } else {
      return <Planets5 />;
    }
  };

  // ---------------Comments-----------------

  const createComment = (content) => {
    setEmptyComment("");
    setDone("");
    const nameOfComment = v4();
    setIdOfComment(nameOfComment);
    const idOfAddon = result.addonId;
    commentAddons(handle, nameOfComment, idOfAddon);

    return addComment(
      content,
      handle,
      nameOfComment,
      idOfAddon,
      profilePicture
    ).then((comment2) => {
      setComments([comment2, ...comments]);
    });
  };
  useEffect(() => {
    if (isNaN(comments.length)) {
      setComments([]);
    }
    if (comments.length === 0) {
      setComment("");
    }

    getAllComments(id).then(setComments).catch(console.error);
  }, [idOfComment, id]);

  // ---------------Favorites-----------------

  const AddRemoveFavorite = () => {
    if (fav === true) {
      removeUserFavorites(userData.handle, id);
      setFav(false);
    } else {
      addUserFavorites(userData.handle, id);
      setFav(true);
    }
  };

  const dateCreatedOn = () => {
    const date = addon?.createdOn;
    if (date !== undefined) {
      return date.toString().split(" ").splice(0, 5).join(" ");
    }
  };

  const descriptionFormat = () => {
    const rowDataDescription = addon?.content?.description;
    if (rowDataDescription !== undefined) {
      const separatedDataDescription = rowDataDescription.split("\n");
      const listDescription = separatedDataDescription.map((row, index) => (
        <p>
          {row}
          <br></br>
        </p>
      ));
      return listDescription;
    }
  };
  const deleteRec = () => {
    deleteAddon(addon.id);
    deleteAddonFile(addon.id);
    deleteAddonImage(addon.id);
    navigate("../addons/all-addons");
  };

  const goToEdit = (id) => {
    verify(id);
    navigate("../addons/not-verified");
  };
  const goToUpdate = (id) => {
    navigate(`/update/${id}`);
  };
  useEffect(() => {
    getAddonById(id).then(setAddon).catch(console.error);

    getFavoriteStatus(id, handle).then(setFav).catch(console.error);

    getRating(id).then(setRatingData).catch(console.error);
  }, [ratingData]);

  return (
    <div className="ViewDetails">
      <div className="ViewDetailsTopContainer">
        <div className="ViewDetailsImage">
          <img
            src={addon?.content?.imageURL}
            alt="Addon"
            className="ViewDetailsImg"
          />
          <div className="DownloadViewDetails">
            <Download id={id} />
          </div>

          <div className="OldVersionsContainer">
            <OldVersions addon={addon} />
          </div>
        </div>

        <div className="RatingComponent">
          <div className="LikeHolder">
            {liked ? (
              <div className="HoverButton">
                <img
                  src={likePressed}
                  alt="like icon"
                  className="LikeButton"
                  onClick={AddLikeAddon}
                />
              </div>
            ) : (
              <div className="HoverButton">
                <img
                  src={like}
                  alt="like icon"
                  className="LikeButton"
                  onClick={AddLikeAddon}
                />
              </div>
            )}
            <p> {likes.length} likes </p>
          </div>

          <div className="LikeHolder">
            {disliked ? (
              <div className="HoverButton">
                <img
                  src={dislikePressed}
                  alt="dislike icon"
                  onClick={AddDislikeAddon}
                />
              </div>
            ) : (
              <div className="HoverButton">
                <img
                  src={dislike}
                  alt="dislike icon"
                  onClick={AddDislikeAddon}
                />
              </div>
            )}
            <p> {dislikes.length} dislikes </p>
          </div>

          <div className="MyRatingComponent">
            <MyRating
              id={id}
              rating={rating}
              setRating={setRating}
              rated={rated}
              setRated={setRated}
              ratingData={ratingData}
              setRatingData={setRatingData}
            />
          </div>

          <div className="LoveHolder">
            {fav ? (
              <>
                <div className="HoverButton">
                  <img
                    src={lovePressed}
                    alt="love icon"
                    onClick={AddRemoveFavorite}
                  />
                </div>
                <p>Remove from favorite</p>
              </>
            ) : (
              <>
                <div className="HoverButton">
                  <img src={love} alt="love icon" onClick={AddRemoveFavorite} />
                </div>
                <p>Add to favorites</p>
              </>
            )}
          </div>
          {userData.role === 3 ? (
            <div className="StarHolder">
              <Featured id={id} />
            </div>
          ) : null}
        </div>
        <div className="DetailsInfo">
          {FillPlanetsBar()}

          <p>
            Name:<span> {addon?.content?.name} </span>
          </p>
          <p>
            Version: <span>{addon?.content?.version}</span>
          </p>
          <p>
            IDE:<span> {addon?.content?.IDE} </span>
          </p>
          <p>
            Author:<span> {addon?.author}</span>
          </p>
          <p>
            Date created: <span>{dateCreatedOn()}</span>
          </p>
          <div className="TagsContainer">
            <p>Tags:</p>
            <div className="tag-container">
              {addon?.content?.tags?.map((e, index) => {
                return (
                  <div key={index} className="tag">
                    {e}
                  </div>
                );
              })}
            </div>
          </div>

          <div className="ButtonsContainer">
            {addon.author === handle && userData.role !== 2 ? (
              <Button
                variant="outline-primary"
                className="ViewDetailsUpdateButton"
                onClick={() => goToUpdate(id)}
              >
                Update addon
              </Button>
            ) : null}

            {(addon.author === handle || userData.role === 3) &&
            userData.role !== 2 ? (
              <Button
                variant="outline-danger"
                className="ViewDetailsDeleteButton"
                onClick={deleteRec}
              >
                Delete
              </Button>
            ) : null}

            {userData?.role === 3 &&
            (addon?.content?.verified === undefined ||
              addon.content.verified === false) ? (
              <Button
                variant="outline-success"
                className="ViewDetailsEditButton"
                onClick={() => goToEdit(id)}
              >
                Verify addon
              </Button>
            ) : null}
          </div>
        </div>
      </div>

      <div className="AddonTextContainer">
        <div className="Description">
          <h2 className="DescriptionName"> Description: </h2>
          <p className="DescriptionText">{descriptionFormat()}</p>
        </div>
      </div>

      <div className="AllComments">
        <AllComments comments={comments} setComments={setComments} />
      </div>

      {userData.role !== 2 ? (
        <div className="InsertComment">
          <CreateComment onSubmit={createComment} />
          <p className="Done">{done}</p>
          <p className="EmptyComment">{emptyComment}</p>
        </div>
      ) : null}
    </div>
  );
};
export default ViewDetails;
