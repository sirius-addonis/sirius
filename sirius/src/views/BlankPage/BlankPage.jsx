import React from "react";
import image from "../../images/blankPageGiphy.gif";
import "./BlankPage.css";

const BlankPage = () => {
  return (
    <div className="greeting">
      <img id="headerImage" src={image} alt="Page Not Found" className="gif" />
      <h1 id="greeting-h1">PAGE NOT FOUND</h1>
    </div>
  );
};

export default BlankPage;
