import React, { useContext, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import LinksToAddons from "../../components/LinksToAddons/LinksToAddons";
import SearchBar from "../../components/SearchBar/SearchBar";
import Users from "../../components/Users/Users";
import AppContext from "../../providers/AppContext";
import { getAllAddons, getAddonById } from "../../services/addons.service";
import { getAllFavorite } from "../../services/users.service";
import { nanoid } from "nanoid";
import "./Favorites.css";
import Download from "../../components/Download/Download";
import MyRating from "../../components/MyRating/MyRating";
import PaginatedItems from "../../components/PaginatedItems/PaginatedItems";

const Favorites = () => {
  const { setContext, ...appState } = useContext(AppContext);
  const {
    userData: { handle },
  } = useContext(AppContext);
  const [addons, setAddons] = useState([]);

  useEffect(() => {
    getAllAddons()
      .then((e) =>
        e.filter((el) => {
          if (
            el.content.addedToFavoritesBy === null ||
            el.content.addedToFavoritesBy === undefined
          ) {
            return null;
          } else if (el.content.addedToFavoritesBy[handle]) {
            return el;
          }
        })
      )
      .then(setAddons)
      .catch(console.error);
  }, []);

  return (
    <div>
      <LinksToAddons />
      <SearchBar />
      <PaginatedItems itemsPerPage={10} addons={addons} />
    </div>
  );
};

export default Favorites;
