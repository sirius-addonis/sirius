import React, { useContext } from "react";
import AppContext from "../../providers/AppContext";
import image from "../../images/surprise.jpg";
import { useNavigate } from "react-router-dom";
import { updateFoundIt } from "../../services/users.service";
import "./Test.css";

const Test = () => {
  const navigate = useNavigate();
  const { userData } = useContext(AppContext);

  const testIt = () => {
    updateFoundIt(userData.handle);
    navigate("../home");
    alert("You have Found The Easter Egg Now You Can Become An Admin");
  };
  return (
    <div className="wellDone">
      <img id="findAWay" src={image} alt="" />
      <p id="here" onClick={testIt}>
        .
      </p>
    </div>
  );
};

export default Test;
