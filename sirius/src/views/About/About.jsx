import React from 'react'
import './About.css';
import Toni from './../../images/Toni.jpg';
import Zhorzh from './../../images/Zhorzh.png';

const About = () => {
  return (
    <div className='About'>
      <h1 id='AboutH1'>Telerik Academy Final Project</h1>
      <h2 id='ProjectName'>&#9733; &#9733; &#9733;   Sirius   &#9733; &#9733; &#9733;</h2>

      <div>
      <div className="authors">
       
          <div className = "singleAuthor">
            <img className="authorImg" src={Toni} alt='Toni'></img>
            <p className="authorName"> Antoniya Lambova </p>
            <a href="mailto:adelfia@gmail.com" className="email">E-mail: adelfia@gmail.com</a>
            <p><a href="tel:+359887985456" className="phone">Phone number: +359 887 985 456</a></p>
          </div>

          <div className = "singleAuthor">
            <img className="authorImg" src={Zhorzh} alt='Toni'></img>
            <p className="authorName"> Zhorzh Tomov </p>
            <a href="mailto:zh_tomov@hotmail.com" className="email">E-mail: zh_tomov@hotmail.com</a>
            <p><a href="tel:+359886854333" className="phone">Phone number: +359 886 854 333</a></p>
          </div>
        </div>
        <div className='AboutProjectInfo'>
          <p>
          Sirius is our final projects as students at Telerik Academy. <br></br>
          It is a Addons Registry single-page web application and we chose to implement it with a space theme.
          </p>
          <p>
              The users must register and log in to have access to all features: 
              <ul>
                <li> view all addons sorted by rating, date, featured and also view their favorite addons </li>
                <li> upload, edit and delete their own addons </li>
                <li> search addon by title, by tag, by IDE </li>
                <li> comment addons </li>
                <li> upvote/downvote the addons that they like or dislike </li>
                <li> each user can be promoted to Admin by another Admin and can search and view details for all users' profiles, have rights to delete all addons and comments, block / unblock users and make other Admins users. </li>
                <li> change light and dark mode </li>
              </ul>
            </p>
            <p>
              The project is made on <span className='AboutInfoSpan'>Node.js</span> with <span className='AboutInfoSpan'>JavaScript</span> and <span className='AboutInfoSpan'>React</span> and for authentication and data storage we used <span className='AboutInfoSpan'>Firebase</span>. 
            </p>
            <p>
               <span id='AboutInfoSpan'> GitLab link:</span> <a href="https://gitlab.com/sirius-addonis/sirius" className='gitLabLink' target="_blank" rel="noreferrer"> https://gitlab.com/sirius-addonis </a>
            </p>
            

        </div>

      </div>


    </div>
  )
}

export default About