import React, { useContext, useEffect, useState } from "react";
import "./Searched.css";
import {
  NavLink,
  useLocation,
  useParams,
  useSearchParams,
} from "react-router-dom";
import LinksToAddons from "../../components/LinksToAddons/LinksToAddons";
import SearchBar from "../../components/SearchBar/SearchBar";
import Users from "../../components/Users/Users";
import { getAllAddons } from "../../services/addons.service";
import { nanoid } from "nanoid";
import MyRating from "../../components/MyRating/MyRating";
import Download from "../../components/Download/Download";
import AppContext from "../../providers/AppContext";

const Searched = () => {
  
  const { setContext, ...appState } = useContext(AppContext);
  const [addons, setAddons] = useState([]);
  const location = useLocation();
  const test = new URLSearchParams(location.search).get("q");

  useEffect(() => {
    getAllAddons().then(setAddons).catch(console.error);
  }, [test]);

  const test2 = addons.filter((el) => {
    if (el === null || el === undefined) {
      return null;
    } else if (el?.content?.name?.toLowerCase().includes(test)) {
      return el;
    } else if (el?.content?.IDE?.toLowerCase().includes(test)) {
      return el;
    } else if (el?.content?.tags?.some((e) => e.toLowerCase().includes(test))) {
      return el;
    }
  });

  return (
    <div>
  
    <LinksToAddons/>
    <SearchBar/>
    <div className='AllAddons'>
      
      {test2.length === 0
      ? <p>No addons to show.</p>
       : 
       
       
       
       test2.map(addon =>  {
       return <div key={addon?.content?.imageURL} className={appState.theme === 'LightTheme' ? "SingleViewAddonContainer" : "SingleViewAddonContainerDark"} >
         
         <NavLink to={`../addons/all-addons/${addon.id}` } className='ViewDetailsLinkAllAddon' >
           <div className='UpperPartGoToViewDetails'>
             <div  className='SingleViewAddon'>
               <h6>{addon?.content?.name}</h6>
               <div className='AddonInfoUp'>
                 <MyRating id={addon.id}></MyRating>
                 <img src={addon?.content?.imageURL} alt='addon' className='SingleImageAddon'></img>
                 <p className='IDE'>IDE: {addon?.content?.IDE}</p>
               </div>
             </div>
           </div>
         </NavLink>


         <div className='DownPartGoToDownloads'>
           <div className='DownloadContainer' >
             <Download id={addon.id} />
           </div>
         </div>

       </div> })

       
      }

    </div>

 </div>
  );
};

export default Searched;
