import "./Home.css";
import React, { useContext, useEffect, useState } from "react";
import Users from "../../components/Users/Users";
import { getAllAddons } from "../../services/addons.service";
import { NavLink, useNavigate } from "react-router-dom";
import AppContext from "../../providers/AppContext";
import image from "./../../../src/images/banner.jpg";
import MyRating from "../../components/MyRating/MyRating";
import Download from "../../components/Download/Download";

const Home = () => {
  const { userData } = useContext(AppContext);
  const { setContext, ...appState } = useContext(AppContext);
  const [addons, setAddons] = useState([]);
  const navigate = useNavigate();

  //alert(appState.theme)

  useEffect(() => {
    getAllAddons().then(setAddons).catch(console.error);
  }, []);

  addons.sort(function (a, b) {
    return +b.content.rating - +a.content.rating;
  });

  const test = addons.filter((e) => {
    if (
      e?.content?.featuredBy === null ||
      e?.content?.featuredBy === undefined
    ) {
      return;
    } else if (Object.keys(e?.content?.featuredBy).length > 0) {
      return e;
    }
  });
  const firstStep = () => {
    navigate("../maybesomedayyouwillfindit");
  };
  return (
    <div className="Home">
      <div className="HeaderImage">
        <img src={image} alt="galaxy" />
        {userData ? <p onClick={firstStep}>.</p> : null}
      </div>
      <Users />

      <h1 className="Title">Top Addons</h1>
      <div className="MyHomeAddons" id="homeAddons">
        {addons
          .sort(function (a, b) {
            return +b.content.rating - +a.content.rating;
          })
          .slice(0, 10)
          .map((addon) => {
            return (
              <>
                <div
                  key={addon?.content?.imageURL}
                  className={
                    appState.theme === "LightTheme"
                      ? "SingleViewAddonContainer"
                      : "SingleViewAddonContainerDark"
                  }
                >
                  <NavLink
                    to={`../addons/all-addons/${addon.id}`}
                    className="ViewDetailsLinkAllAddon"
                  >
                    <div className="UpperPartGoToViewDetails">
                      <div className="SingleViewAddon">
                        <h6>{addon?.content?.name}</h6>
                        <div className="AddonInfoUp">
                          <MyRating id={addon.id}></MyRating>
                          <img
                            src={addon?.content?.imageURL}
                            alt="addon"
                            className="SingleImageAddon"
                          ></img>
                          <p className="IDE">IDE: {addon?.content?.IDE}</p>
                        </div>
                      </div>
                    </div>
                  </NavLink>

                  <div className="DownPartGoToDownloads">
                    <div className="DownloadContainer">
                      <Download id={addon.id} />
                    </div>
                  </div>
                </div>
              </>
            );
          })}
      </div>

      <h1 className="Title">Featured Addons</h1>
      <div className="MyHomeAddons" id="homeAddons">
        {test
          .sort(function (a, b) {
            return +b?.content?.featuredBy - +a?.content?.featuredBy;
          })
          .slice(0, 10)
          .map((addon) => {
            return (
              <>
                <div
                  key={addon?.content?.imageURL}
                  className={
                    appState.theme === "LightTheme"
                      ? "SingleViewAddonContainer"
                      : "SingleViewAddonContainerDark"
                  }
                >
                  <NavLink
                    to={`../addons/all-addons/${addon.id}`}
                    className="ViewDetailsLinkAllAddon"
                  >
                    <div className="UpperPartGoToViewDetails">
                      <div className="SingleViewAddon">
                        <h6>{addon?.content?.name}</h6>
                        <div className="AddonInfoUp">
                          <MyRating id={addon.id}></MyRating>
                          <img
                            src={addon?.content?.imageURL}
                            alt="addon"
                            className="SingleImageAddon"
                          ></img>
                          <p className="IDE">IDE: {addon?.content?.IDE}</p>
                        </div>
                      </div>
                    </div>
                  </NavLink>

                  <div className="DownPartGoToDownloads">
                    <div className="DownloadContainer">
                      <Download id={addon.id} />
                    </div>
                  </div>
                </div>
              </>
            );
          })}
      </div>

      <h1 className="Title">New Addons</h1>
      <div className="MyHomeAddons">
        {addons
          .sort(function (a, b) {
            return b.createdOn - a.createdOn;
          })
          .slice(0, 10)
          .map((addon) => {
            return (
              <>
                <div
                  key={addon?.content?.imageURL}
                  className={
                    appState.theme === "LightTheme"
                      ? "SingleViewAddonContainer"
                      : "SingleViewAddonContainerDark"
                  }
                >
                  <NavLink
                    to={`../addons/all-addons/${addon.id}`}
                    className="ViewDetailsLinkAllAddon"
                  >
                    <div className="UpperPartGoToViewDetails">
                      <div className="SingleViewAddon">
                        <h6>{addon?.content?.name}</h6>
                        <div className="AddonInfoUp">
                          <MyRating id={addon.id}></MyRating>
                          <img
                            src={addon?.content?.imageURL}
                            alt="addon"
                            className="SingleImageAddon"
                          ></img>
                          <p className="IDE">IDE: {addon?.content?.IDE}</p>
                        </div>
                      </div>
                    </div>
                  </NavLink>

                  <div className="DownPartGoToDownloads">
                    <div className="DownloadContainer">
                      <Download id={addon.id} />
                    </div>
                  </div>
                </div>
              </>
            );
          })}
      </div>
    </div>
  );
};

export default Home;
