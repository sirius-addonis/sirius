import React, { useState, useContext, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  addAddon,
  getAddonById,
  updateOldVers,
} from "../../services/addons.service";
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";
import { storage } from "../../config/firebase-config";
import "./UpdateAddon.css";
import AppContext from "../../providers/AppContext";
import { Tags } from "../../components/Tags/Tags";

const UpdateAddon = () => {
  const { userData } = useContext(AppContext);
  const [addon, setAddon] = useState({});
  const [addonImage, setAddonImage] = useState();
  const [addonFile, setAddonFile] = useState();
  const [preview, setPreview] = useState();
  const navigate = useNavigate();
  const [form, setForm] = useState({
    name: "",
    IDE: "",
    description: "",
    tags: new Set(),
    rating: 0,
    imageURL: "",
    addonFileURL: "",
    commentsIds: "",
    ratedBy: "",
    addedToFavoritesBy: "",
    commentedBy: "",
    version: "1.0.0",
    oldVersions: [],
    downloads: 0,
    verified: false,
  });
  const [wrongFileURL, setWrongFileURL] = useState("");
  const [wrongImageURL, setWrongImageURL] = useState("");
  const [wrongName, setWrongName] = useState("");
  const [wrongIDE, setWrongIDE] = useState("");
  const [wrongDescription, setWrongDescription] = useState("");
  const [wrongTags, setWrongTags] = useState("");
  const { id } = useParams();
  useEffect(() => {
    getAddonById(id).then(setAddon).catch(console.error);
  }, []);
  useEffect(() => {
    setForm({
      name: addon?.content?.name,
      IDE: addon?.content?.IDE,
      description: addon?.content?.description,
      tags: addon?.content?.tags,
      rating: addon?.content?.rating,
      imageURL: addon?.content?.imageURL,
      addonFileURL: addon?.content?.addonFileURL,
      commentsIds:
        addon?.content?.commentsIds !== undefined
          ? addon?.content?.commentsIds
          : [],
      ratedBy:
        addon?.content?.ratedBy !== undefined ? addon?.content?.ratedBy : "",
      addedToFavoritesBy:
        addon?.content?.addedToFavoritesBy !== undefined
          ? addon?.content?.addedToFavoritesBy
          : [],
      commentedBy:
        addon?.content?.commentedBy !== undefined
          ? addon?.content?.commentedBy
          : [],
      version: addon?.content?.version,
      oldVersions:
        addon?.content?.oldVersions !== undefined
          ? addon?.content?.oldVersions
          : [],
      verified:
        addon?.content?.verified !== undefined
          ? addon?.content?.verified
          : false,
      downloads:
        addon?.content?.downloads !== undefined ? addon?.content?.downloads : 0,
    });
  }, [addon]);

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const register = (e) => {
    e.preventDefault();
    if (form.name.length < 5 || form.name.length > 64) {
      setWrongName("* Please add a name between 5 and 64 symbols!");
      return;
    }
    if (form.IDE.length < 5 || form.IDE.length > 64) {
      setWrongIDE("* Please add IDE between 5 and 64 symbols!");
      return;
    }
    if (form.description.length < 5 || form.description.length > 8000) {
      setWrongDescription("* Please add description between 5 and 64 symbols!");
      return;
    }
    if (form.tags.length < 1 || form.tags.length > 30) {
      setWrongTags("* Please add tags between 1 and 10");
      return;
    }

    if (form.addonFileURL === "") {
      setWrongFileURL("* Please insert file");
      return;
    }
    let addOldVers = [];

    if (!form.oldVersions) {
      addOldVers = [
        {
          version: addon?.content?.version,
          date: addon?.createdOn,
          URLofVersion: addon?.content?.addonFileURL,
        },
      ];
    } else {
      addOldVers = form?.oldVersions?.push({
        version: addon?.content?.version,
        date: addon?.createdOn,
        URLofVersion: addon?.content?.addonFileURL,
      });
    }
    updateOldVers(id, addOldVers);
    setForm({
      ...form,
      oldVersions: addOldVers,
    });

    const picture = ref(storage, `images/${id}`);
    let url12 = "";
    let url13 = "";
    uploadBytes(picture, addonImage).then((snapshot) => {
      return getDownloadURL(snapshot.ref)
        .then((url) => {
          url12 = url;
        })
        .then(() => {
          const fileREf = ref(
            storage,
            `files/${id}/${form.version}/${addonFile.name}`
          );

          uploadBytes(fileREf, addonFile)
            .then((snapshot) => {
              return getDownloadURL(snapshot.ref)
                .then((url) => {
                  url13 = url;
                })
                .then(() => {
                  let form2 = { ...form, imageURL: url12, addonFileURL: url13 };

                  addAddon(form2, userData.handle, id).then((addon) => {
                    navigate(`../addons/all-addons/${id}`);
                  });
                });
            })

            .catch(console.error);
        });
    });
  };
  const uploadPicture = (e) => {
    e.preventDefault();
    setAddonImage(e.target?.files?.[0]);
  };
  const uploadFile = (e) => {
    e.preventDefault();
    setAddonFile(e.target?.files?.[0]);
  };
  useEffect(() => {
    if (!addonImage) {
      setPreview("");
      return;
    }
    const userLogoUrl = URL.createObjectURL(addonImage);
    setPreview(userLogoUrl);
    return () => URL.revokeObjectURL(userLogoUrl);
  }, [addonImage]);
  return (
    <div className="UploadBox">
      <div className="UploadUpperPart">
        <div className="InsertBar">
          <div className="VersionInput">
            <input
              type="text"
              id="version"
              name="version"
              placeholder={form.version}
              onChange={updateForm("version")}
            ></input>
          </div>
          <div className="NameInput">
            <input
              type="text"
              id="name"
              name="name-input"
              label="Step 1"
              value={form.name}
              placeholder="Insert Name of Addon"
              onChange={updateForm("name")}
            ></input>
            <p className="wrongInput">{wrongName}</p>
          </div>
          <div className="DescriptionInput">
            <textarea
              type="text"
              id="description"
              name="description-input"
              value={form.description}
              placeholder="Insert description of Addon"
              onChange={updateForm("description")}
            ></textarea>
            <p className="wrongInput">{wrongDescription}</p>
          </div>
        </div>
        <div className="FileBox">
          <div className="AddonFile1">
            <p className="wrongInput">{wrongFileURL}</p>
            <input type="file" name="file" onChange={uploadFile} />
          </div>

          <div className="UploadImage1">
            {!addonImage ? (
              <div>
                <img
                  src={form.imageURL}
                  alt="change"
                  className="UploadImage1"
                />
                <p className="wrongInput">{wrongImageURL}</p>
                <input type="file" name="file" onChange={uploadPicture} />
              </div>
            ) : (
              <>
                {" "}
                <img src={preview} alt="change" className="UploadImage1" />
                <p className="wrongInput">{wrongImageURL}</p>
                <input type="file" name="file" onChange={uploadPicture} />
              </>
            )}
          </div>

          <div className="TagContainer">
            <Tags form={form} setForm={setForm} />
          </div>
        </div>
      </div>

      <button className="ProfileEditButton3" type="submit" onClick={register}>
        Upload
      </button>
    </div>
  );
};

export default UpdateAddon;
