import React, { useState, useContext, useEffect } from "react";
import "./Upload.css";
import image from "../../images/logo.jpg";
import AppContext from "../../providers/AppContext";
import { useNavigate } from "react-router-dom";
import { storage } from "../../config/firebase-config";
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";
import { v4 } from "uuid";
import { addAddon } from "../../services/addons.service";
import { Tags } from "../../components/Tags/Tags";

const Upload = () => {
  const { userData } = useContext(AppContext);
  const [addons, setAddons] = useState([]);
  const [idOfAddon, setIdOfAddon] = useState("");
  const [addonFile, setAddonFile] = useState();
  const [preview, setPreview] = useState();
  const [addonImage, setAddonImage] = useState();
  const navigate = useNavigate();
  const [form, setForm] = useState({
    name: "",
    IDE: "",
    description: "",
    tags: new Set(),
    rating: 0,
    imageURL: image,
    addonFileURL: "",
    commentsIds: "",
    likedBy: "",
    dislikedBy: "",
    addedToFavoritesBy: "",
    commentedBy: "",
    version: "1.0.0",
    oldVersions: [],
    verified: false,
    downloads: 0,
    featuredBy: "",
  });
  const [wrongFileURL, setWrongFileURL] = useState("");
  const [wrongImageURL, setWrongImageURL] = useState("");
  const [wrongName, setWrongName] = useState("");
  const [wrongIDE, setWrongIDE] = useState("");
  const [wrongDescription, setWrongDescription] = useState("");
  const [wrongTags, setWrongTags] = useState("");

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const register = (e) => {
    e.preventDefault();

    if (form.name.length < 5 || form.name.length > 64) {
      setWrongName("* Please add a name between 5 and 64 symbols!");
      return;
    }
    if (form.IDE.length < 5 || form.IDE.length > 64) {
      setWrongIDE("* Please add IDE between 5 and 64 symbols!");
      return;
    }
    if (form.description.length < 5 || form.description.length > 112000) {
      setWrongDescription("* Please add description between 5 and 64 symbols!");
      return;
    }
    if (form.tags.length < 1 || form.tags.length > 10) {
      setWrongTags("* Please add tags between 1 and 10");
      return;
    }

    if (!addonImage) {
      setWrongImageURL("* Please insert picture");
      return;
    }
    if (!addonFile) {
      setWrongFileURL("* Please insert file");
      return;
    }

    const picture = ref(storage, `images/${idOfAddon}`);
    let url12 = "";
    let url13 = "";
    uploadBytes(picture, addonImage).then((snapshot) => {
      return getDownloadURL(snapshot.ref)
        .then((url) => {
          url12 = url;
        })
        .then(() => {
          const fileREf = ref(
            storage,
            `files/${idOfAddon}/${form.version}/${addonFile.name}`
          );

          uploadBytes(fileREf, addonFile)
            .then((snapshot) => {
              return getDownloadURL(snapshot.ref)
                .then((url) => {
                  url13 = url;
                })
                .then(() => {
                  let form2 = { ...form, imageURL: url12, addonFileURL: url13 };

                  addAddon(form2, userData.handle, idOfAddon).then((addon) => {
                    setAddons([addon, ...addons]);
                    navigate(`../addons/all-addons/${idOfAddon}`);
                  });
                });
            })

            .catch(console.error);
        });
    });
  };
  const uploadPicture = (e) => {
    e.preventDefault();
    setAddonImage(e.target?.files?.[0]);
  };
  const uploadFile = (e) => {
    e.preventDefault();
    setAddonFile(e.target?.files?.[0]);
  };

  useEffect(() => {
    const nameOfAddon = v4();
    setIdOfAddon(nameOfAddon);
  }, []);
  useEffect(() => {
    if (!addonImage) {
      setPreview("");
      return;
    }
    const userLogoUrl = URL.createObjectURL(addonImage);
    setPreview(userLogoUrl);
    return () => URL.revokeObjectURL(userLogoUrl);
  }, [addonImage]);

  return (
    <div className="UploadBox1">
      <div className="UploadUpperPart">
        <div className="InsertBar1">
          <div className="NameInput1">
            <input
              type="text"
              id="name1"
              name="name-input"
              placeholder="Insert Name of Addon"
              onChange={updateForm("name")}
            ></input>
            <p className="wrongInput">{wrongName}</p>
          </div>
          <div className="IDEInput1">
            <input
              type="text"
              id="IDE1"
              name="IDE-input"
              placeholder="Insert IDE of Addon"
              onChange={updateForm("IDE")}
            ></input>
            <p className="wrongInput">{wrongIDE}</p>
          </div>
          <div className="DescriptionInput1">
            <textarea
              id="description1"
              name="description-input"
              placeholder="Insert description of Addon"
              onChange={updateForm("description")}
            ></textarea>
            <p className="wrongInput">{wrongDescription}</p>
          </div>

        </div>

        <div className="FileBox1">
          <div className="AddonFile1">
            <p className="wrongInput">{wrongFileURL}</p>
            <input type="file" name="file" onChange={uploadFile} />
          </div>

          <div className="UploadImage1">
            {!addonImage ? (
              <div>
                <img src={image} alt="change" className="UploadImage1" />
                <p className="wrongInput">{wrongImageURL}</p>
                <input type="file" name="file" onChange={uploadPicture} />
              </div>
            ) : (
              <>
                {" "}
                <img src={preview} alt="change" className="UploadImage1" />
                <p className="wrongInput">{wrongImageURL}</p>
                <input type="file" name="file" onChange={uploadPicture} />
              </>
            )}

            
          </div>

          <div className="TagContainer">
            <Tags form={form} setForm={setForm} />
          </div>

     
        </div>

      </div>
      <div className="UploadButtonContainer">
        <button className="ProfileEditButton3" type="submit" onClick={register}>
          Upload
        </button>
      </div>
    </div>
  );
};

export default Upload;
