import React, { useEffect, useState, useContext } from "react";
import LinksToAddons from "../../components/LinksToAddons/LinksToAddons";
import PaginatedItems from "../../components/PaginatedItems/PaginatedItems";
import SearchBar from "../../components/SearchBar/SearchBar";
import { getAllAddons } from "../../services/addons.service";
import AppContext from "../../providers/AppContext";
import "./AllAddons.css";

const AllAddons = () => {
  const { setContext, ...appState } = useContext(AppContext);
  const [addons, setAddons] = useState([]);
  //alert(appState.theme)
  useEffect(() => {
    getAllAddons().then(setAddons).catch(console.error);
  }, []);
  const test = addons.filter((e) => e.content.verified === true);

  return (
    <div>
      <LinksToAddons />
      <SearchBar />
      <PaginatedItems itemsPerPage={10} addons={test} />
    </div>
  );
};

export default AllAddons;
