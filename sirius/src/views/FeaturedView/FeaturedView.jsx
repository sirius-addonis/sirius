import React, { useContext, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import Download from "../../components/Download/Download";
import LinksToAddons from "../../components/LinksToAddons/LinksToAddons";
import MyRating from "../../components/MyRating/MyRating";
import SearchBar from "../../components/SearchBar/SearchBar";
import Users from "../../components/Users/Users";
import { getAllAddons } from "../../services/addons.service";
import { nanoid } from "nanoid";
import AppContext from "../../providers/AppContext";
import PaginatedItems from "../../components/PaginatedItems/PaginatedItems";

const FeaturedView = () => {
  const { setContext, ...appState } = useContext(AppContext);
  const [addons, setAddons] = useState([]);

  useEffect(() => {
    getAllAddons().then(setAddons).catch(console.error);
  }, []);
  const test = addons.filter((e) => {
    if (
      e?.content?.featuredBy === null ||
      e?.content?.featuredBy === undefined
    ) {
      return e;
    } else if (Object.keys(e?.content?.featuredBy).length > 0) {
      return e;
    }
  });

  return (
    <div>
      <LinksToAddons />
      <SearchBar />
      <PaginatedItems itemsPerPage={10} addons={test} />
    </div>
  );
};

export default FeaturedView;
