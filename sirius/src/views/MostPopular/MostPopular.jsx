import React, { useContext, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import Download from "../../components/Download/Download";
import LinksToAddons from "../../components/LinksToAddons/LinksToAddons";
import MyRating from "../../components/MyRating/MyRating";
import PaginatedItems from "../../components/PaginatedItems/PaginatedItems";
import SearchBar from "../../components/SearchBar/SearchBar";
import Users from "../../components/Users/Users";
import AppContext from "../../providers/AppContext";
import { getAllAddons } from "../../services/addons.service";

const MostPopular = () => {
  const { setContext, ...appState } = useContext(AppContext);
  const [addons, setAddons] = useState([]);

  useEffect(() => {
    getAllAddons().then(setAddons).catch(console.error);
  }, []);
  const test = addons
    .sort(function (a, b) {
      return +b.content.rating - +a.content.rating;
    })
    .filter((e) => e.content.verified === true);
  return (
    <div>
      <LinksToAddons />
      <SearchBar />
      <PaginatedItems itemsPerPage={10} addons={test} />
    </div>
  );
};

export default MostPopular;
