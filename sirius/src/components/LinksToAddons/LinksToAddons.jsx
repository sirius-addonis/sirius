import "./LinksToAddons.css";
import React, { useEffect, useState, useContext } from "react";
import { createSearchParams, NavLink, useNavigate } from "react-router-dom";
import star from "../../images/star-allAddons.png";
import AppContext from "../../providers/AppContext";

const LinksToAddons = () => {
  const { user, userData } = useContext(AppContext);
  const [category, setCategory] = useState("Category");
  const [form, setForm] = useState("");
  const navigate = useNavigate();

  useEffect(
    (e) => {
      if (category !== "Category") {
        let params = new URLSearchParams(`q=${category}`);
        navigate({
          pathname: `./../category`,
          search: `${createSearchParams(params)}`,
        });
        // navigate(`/category`, {state:{category: category}})
      }
    },
    [category]
  );

  return (
    <div>
      <div className="NavLinksContainer">
        <NavLink to={"../addons/all-addons"} className="navLink">
          All Addons
        </NavLink>
        <NavLink to={"../addons/featured"} className="navLink">
          Featured
        </NavLink>
        {user !== null ? (
          <NavLink to={"../addons/favorites"} className="navLink">
            Favorites
          </NavLink>
        ) : null}
        <NavLink to={"../addons/most-popular"} className="navLink">
          Most Popular
        </NavLink>
        <NavLink to={"../addons/new-addons"} className="navLink">
          New Addons
        </NavLink>
        {userData?.role === 3 ? (
          <NavLink to={"../addons/not-verified"} className="navLink">
            To Be Verified
          </NavLink>
        ) : null}
      </div>
    </div>
  );
};

export default LinksToAddons;
