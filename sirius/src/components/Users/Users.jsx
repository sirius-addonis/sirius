import React, { useEffect, useState } from 'react'
import { getAllAddons } from '../../services/addons.service'
import { getAllUsers } from '../../services/users.service'
import './Users.css'

const Users = () => {
  const [users, setUsers] = useState([])
  const [addons, setAddons] = useState([])

  useEffect (() => {
    getAllUsers()
    .then(setUsers)
    .catch(console.error)

    getAllAddons()
    .then(setAddons)
    .catch(console.error)
  }, [])

 


  
  return (
    <div className="Users">
        <div className="UsersCounts">Number of Users: {users.length}</div>   
        <div className="PostsCounts">Number of Addonis: {addons.length}</div>
    </div>
  )
}

export default Users