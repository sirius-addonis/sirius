import './Planets.css';
import React from "react";
import empty1  from './../../../src/images/Planet1-empty.png';
import empty2  from './../../../src/images/Planet2-empty.png';
import empty3  from './../../../src/images/Planet3-empty.png';
import empty4  from './../../../src/images/Planet4-empty.png';
import empty5  from './../../../src/images/Planet5-empty.png';

 

const Planets0 = () => {


  return (
    <div className="Planets">
      <img src={empty1} alt='Empty planets'/>
      <img src={empty2} alt='Empty planets'/>
      <img src={empty3} alt='Empty planets'/>
      <img src={empty4} alt='Empty planets'/>
      <img src={empty5} alt='Empty planets'/>

    </div>
  );
}

export default Planets0
