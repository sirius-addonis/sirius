import './Planets.css';
import React from "react";
import fill1  from './../../../src/images/Planet1.png';
import fill2  from './../../../src/images/Planet2.png';
import fill3  from './../../../src/images/Planet3.png';
import empty4  from './../../../src/images/Planet4-empty.png';
import empty5  from './../../../src/images/Planet5-empty.png';

 

const Planets3 = () => {


  return (
    <div className="Planets">
      <img src={fill1} alt='Filled planet'/>
      <img src={fill2} alt='Filled planets'/>
      <img src={fill3} alt='Filled planets'/>
      <img src={empty4} alt='Empty planets'/>
      <img src={empty5} alt='Empty planets'/>

    </div>
  );
}

export default Planets3