import React, { useState, useEffect } from "react";
import "./Tags.css";

export const Tags = (props) => {
  const { form, setForm } = props;
  const [tags, setTags] = useState([]);
  const [wrongTag, setWrongTag] = useState("");

  const addTags = (e) => {
    if (
      e.key === "Enter" &&
      e.target.value.length > 0 &&
      !tags.includes(e.target.value)
    ) {
      setTags([...tags, e.target.value]);
      e.target.value = "";
    }
    if (tags.includes(e.target.value)) {
      setWrongTag("* Please insert unique tag");
    }
  };
  const removeTag = (tagToRemove) => {
    const newTags = tags.filter((tag) => tag !== tagToRemove);
    setTags(newTags);
  };
  useEffect(() => {
    setForm({ ...form, tags: tags });
  }, [tags]);
  return (
    <div>
      <div className="tag-container">
        {tags.map((e, index) => {
          return (
            <div key={index} className="tag">
              {e} <span onClick={() => removeTag(e)}>x</span>
            </div>
          );
        })}

        <input onKeyDown={addTags} placeholder="insert tag and press Enter" />
      </div>
      <p className="wrongInput">{wrongTag}</p>
    </div>
  );
};
