import React, { useEffect, useState } from "react";
import "./MyRating.css";
import { Box, Rating } from "@mui/material";
import { getRating } from "../../services/addons.service";

const MyRating = ({ id }) => {
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const [ratingData, setRatingData] = useState(false);

  useEffect(() => {
    setLoading(true);
    getRating(id)
      .then(setRatingData)
      .catch((e) => setError(e.message))
      .finally(setLoading(false));
  }, []);

  if (error) {
    return <div> Error...</div>;
  }

  if (loading) {
    return <div> Loading data...</div>;
  }

  let rating;
  if (ratingData === 0) {
    rating = 0;
  } else if (ratingData < 20) {
    rating = 1;
  } else if (ratingData < 40) {
    rating = 2;
  } else if (ratingData < 60) {
    rating = 3;
  } else if (ratingData < 80) {
    rating = 4;
  } else {
    rating = 5;
  }

  return (
    <div className="RateHolder">
      {ratingData === undefined ? (
        <>
          <p>Calculating...</p>
        </>
      ) : (
        <>
          <Box
            sx={{
              "& > legend": { mt: 2 },
            }}
          >
            <Rating name="simple-controlled" value={rating} readOnly />
          </Box>
        </>
      )}
    </div>
  );
};

export default MyRating;
