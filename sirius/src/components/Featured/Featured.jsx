import React, { useState, useContext, useEffect } from "react";
import star from "./../../../src/images/star.png";
import starPressed from "./../../../src/images/star-pressed.png";
import AppContext from "../../providers/AppContext";
import {
  addUserFeatured,
  removeUserFeatured,
} from "../../services/users.service";
import { getFeaturedStatus } from "../../services/addons.service";

const Featured = (props) => {
  const { id } = props;
  const { userData } = useContext(AppContext);
  const [featured, setFeatured] = useState(false);
  const AddRemoveFeatured = () => {
    if (featured === true) {
      removeUserFeatured(userData.handle, id);
      setFeatured(false);
    } else {
      addUserFeatured(userData.handle, id);
      setFeatured(true);
    }
  };

  useEffect(() => {
    getFeaturedStatus(id, userData.handle)
      .then(setFeatured)
      .catch(console.error);
  }, []);
  return (
    <div className="StarHolder">
      {featured ? (
        <>
         <div className="HoverButton">
            <img src={starPressed} alt="star icon" onClick={AddRemoveFeatured} />
          </div>
          <p>Remove from featured</p>
        </>
      ) : (
        <>
         <div className="HoverButton">
            <img src={star} alt="star icon" onClick={AddRemoveFeatured} />
          </div>
          <p>Add to featured</p>
        </>
      )}
    </div>
  );
};

export default Featured;
