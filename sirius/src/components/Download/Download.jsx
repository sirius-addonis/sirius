import React, { useState, useEffect } from "react";
import "./Download.css";
import { counterUpdate, getAddonById } from "../../services/addons.service";
import downloadButton from "./../../../src/images/download.png";

const Download = ({ id }) => {
  const [addon, setAddon] = useState({});
  const [counter, setCounter] = useState();

  useEffect(() => {
    getAddonById(id).then(setAddon).catch(console.error);
  }, []);
  useEffect(() => {
    setCounter(addon?.content?.downloads);
  }, [addon]);
  const handleClick = () => {
    setCounter(counter + 1);
    counterUpdate(addon.id, counter);
  };

  return (
    <div className="DownloadButton">
      <a href={addon?.content?.addonFileURL} onClick={handleClick}>
        {/* Download Here{" "} */}
        {"   "}
        <img src={downloadButton} alt="Download button" /> {"   "}
        <p className="DownloadViewDetailsCounter">Downloaded: {counter} times </p>
      </a>

    </div>
  );
};

export default Download;
