import React from "react";
import "./OldVersions.css";

const OldVersions = (props) => {
  const { addon } = props;

  let results = [];
  if (!addon?.content?.oldVersions) {
    results = [];
  } else {
    results = addon.content.oldVersions;
  }

  return (
    <div className="table">
      {results.length === 0 ? (
        <p className="NoOldVersions">No Old Versions</p>
      ) : (
        <table>
          <thead>
            <tr>
              <th className="version">Old Versions</th>
              <th className="download1">Download Link</th>
            </tr>
          </thead>
          <tbody>
            {results.map((val, key) => {
              return (
                <tr key={key}>
                  <td className="version">Version: {val.version}</td>
                  <td className="download">
                    <a href={val.URLofVersion}>download here</a>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
    </div>
  );
};

export default OldVersions;
