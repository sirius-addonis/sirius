import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import AppContext from "../../providers/AppContext";
import Download from "../Download/Download";
import MyRating from "../MyRating/MyRating";

const AddonsToShow = ({ currentAddons }) => {

  const { setContext, ...appState } = useContext(AppContext);

  return (
    <>
      <div className="AllAddons">
        {currentAddons?.map((addon) => {
          return (
            <div
              key={addon?.content?.imageURL}
              className={appState.theme === 'LightTheme' ? "SingleViewAddonContainer" : "SingleViewAddonContainerDark"}
            >
              <NavLink
                to={`../addons/all-addons/${addon.id}`}
                className="ViewDetailsLinkAllAddon"
              >
                <div className="UpperPartGoToViewDetails">
                  <div className="SingleViewAddon">
                    <h6>{addon?.content?.name}</h6>
                    <div className="AddonInfoUp">
                      <MyRating id={addon.id}></MyRating>
                      <img
                        src={addon?.content?.imageURL}
                        alt="addon"
                        className="SingleImageAddon"
                      ></img>
                      <p className="IDE">IDE: {addon?.content?.IDE}</p>
                    </div>
                  </div>
                </div>
              </NavLink>

              <div className="DownPartGoToDownloads">
                <div className="DownloadContainer">
                  <Download id={addon.id} />
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default AddonsToShow;
