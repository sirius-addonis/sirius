import React, { useState, useEffect } from "react";
import AddonsToShow from "../AddonsToShow/AddonsToShow";
import ReactPaginate from "react-paginate";
import "./PaginatedItems.css";

const PaginatedItems = ({ itemsPerPage, addons }) => {
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);

  useEffect(() => {
    const endOffset = itemOffset + itemsPerPage;
    setCurrentItems(addons.slice(itemOffset, endOffset));
    setPageCount(Math.ceil(addons.length / itemsPerPage));
  }, [itemOffset, itemsPerPage, addons]);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * itemsPerPage) % addons.length;

    setItemOffset(newOffset);
  };

  return (
    <>
      <AddonsToShow currentAddons={currentItems} />
      <div className="pagination">
        <ReactPaginate
          breakLabel="..."
          nextLabel="next >"
          onPageChange={handlePageClick}
          pageRangeDisplayed={5}
          pageCount={pageCount}
          previousLabel="< previous"
          renderOnZeroPageCount={null}
          breakClassName={"page-item"}
          breakLinkClassName={"page-link"}
          containerClassName={"pagination"}
          pageClassName={"page-item"}
          pageLinkClassName={"page-link"}
          previousClassName={"page-item"}
          previousLinkClassName={"page-link"}
          nextClassName={"page-item"}
          nextLinkClassName={"page-link"}
          activeClassName={"active"}
        />
      </div>
    </>
  );
};

export default PaginatedItems;
