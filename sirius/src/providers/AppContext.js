import { createContext } from 'react';

export function getColorMode() {
  const mode = localStorage.getItem('colorMode');
  return mode ? mode : 'LightTheme';
}

export function setColorMode(mode) {
    localStorage.setItem('colorMode', JSON.stringify(mode));
}



const AppContext = createContext({
  user: null,
  userData: null,
  theme: getColorMode,
  setContext() {
    // real implementation comes from App.jsx
  },
});

export default AppContext;
