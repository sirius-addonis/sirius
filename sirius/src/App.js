
import './App.css';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppContext, { getColorMode } from './providers/AppContext';
import { useEffect, useState } from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from './config/firebase-config';
import { getUserData } from './services/users.service';
import Authenticated from './hoc/Authenticated';
import Home from './views/Home/Home';
import About from './views/About/About';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Register from './views/Register/Register';
import Login from './views/Login/Login';
import Upload from './views/Upload/Upload';
import Profile from './views/Profile/Profile';
import EditProfile from './views/EditProfile/EditProfile';
import AllAddons from './views/AllAddons/AllAddons';
import Favorites from './views/Favorites/Favorites';
import MostPopular from './views/MostPopular/MostPopular';
import New from './views/New/New';
import ViewDetails from './views/ViewDetails/ViewDetails';
import UpdateAddon from './views/UpdateAddon/UpdateAddon';
import MyAddons from './views/MyAddons/MyAddons';
import AdminUsers from './views/AdminUsers/AdminUsers';
import NotVerified from './views/NotVerified/NotVerified';
import SearchedUsers from './views/SearchedUsers/SearchedUsers';
import Searched from './views/Searched/Searched'
import { Tags } from './components/Tags/Tags';
import FeaturedView from './views/FeaturedView/FeaturedView';
import UsersInfo from './views/UsersInfo/UsersInfo'
import BlankPage from './views/BlankPage/BlankPage';
import Test from './views/Test/Test';




function App() {

  const [appState, setAppState] = useState({
    user: null,
    userData: null,
    theme: getColorMode(),
  });

  let [user] = useAuthState(auth);


  useEffect(() => {
    if (user === null) return;

    getUserData(user.uid)
      .then(snapshot => {
        if (!snapshot.exists()) {
          throw new Error('Something went wrong!');
        }

        setAppState({
          user,
          userData: snapshot.val()[Object.keys(snapshot.val())[0]],
          theme: JSON.parse(localStorage.getItem('darkMode')),
        });


      })
      .catch(e => console.log(e.message));
  }, [user]);


  return (
    
    <BrowserRouter>
      <AppContext.Provider value={{...appState, setContext: setAppState}}>
        <div className={appState.theme === 'LightTheme' ? "App" : "AppDark"}>
        {/* <div className={color === 'LightTheme' ? "App" : "AppDark"}> */}
          <Header/>
          <div className='Outlet'>
            <Routes>
            <Route index element={<Navigate replace to="home" />} />
              <Route path="home" element ={<Home/>}></Route>

              {/* 
              <Route path="my-addons" element={<MyAddons/>}></Route>
              <Route path="upload" element={<Upload/>}></Route>
              <Route path="edit/:id" element={<Edit/>}></Route>
              
              <Route path="addons/search" element={<Searched />}/>
              <Route path="addons/category" element={<Category />}/>
              <Route path="users" element={<AdminUsers />}/>
              <Route path="users/search" element={<SearchedUsers/>}/>
              <Route path="/users/:id" element={<UsersInfo/>}></Route>
              
              */}


              <Route path="addons" index element ={<Navigate replace to="all-addons"/>}></Route>
              <Route path="addons/all-addons" element={<AllAddons/>}></Route>
              <Route path="addons/favorites" element={<Favorites/>}></Route>
              <Route path="addons/most-popular" element={<MostPopular/>}></Route>
              <Route path="addons/new-addons" element={<New/>}></Route>
              <Route path='addons/not-verified' element={<NotVerified/>}></Route>
              <Route path='addons/featured' element = {<FeaturedView/>}></Route>
              <Route path="edit-profile" element={<EditProfile/>}></Route>
              <Route path="addons/search" element={<Searched />}/>
              <Route path="profile" element={<Profile />} />
              <Route path="register" element={<Register />} />
              <Route path="login" element={<Login />} />
              <Route path="about" element={<About />} />
              <Route path="my-addons" element={<MyAddons/>}/>
              <Route path="upload" element={<Upload/>}></Route>
              <Route path="addons/all-addons/:id" element={<Authenticated> <ViewDetails /> </Authenticated>} ></Route>
              <Route path="update/:id" element={<UpdateAddon/>}></Route>
              <Route path="users" element={<AdminUsers />}/>
              <Route path="users/search" element={<SearchedUsers/>}/> 
              <Route path='tags' element= {<Tags/>}></Route>
              <Route path="/users/:id" element={<UsersInfo/>}></Route>
              <Route path="*" element={<BlankPage/>}/>
              <Route path="maybesomedayyouwillfindit" element= {<Test/>}/>
              
              
              
              
            </Routes>
          </div>
          <footer>
            <Footer/>
          </footer>
        </div>
      </AppContext.Provider>
    </BrowserRouter>
  );
}

export default App;
