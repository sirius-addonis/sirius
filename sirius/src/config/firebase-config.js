import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getFirestore } from '@firebase/firestore';
import { getDatabase } from '@firebase/database';
import { getStorage } from 'firebase/storage';






const firebaseConfig = {
  apiKey: "AIzaSyCxqVYU78aE5DvMy1Nvq8EOer-wDngU0dg",
  authDomain: "sirius-addonis.firebaseapp.com",
  databaseURL: "https://sirius-addonis-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "sirius-addonis",
  storageBucket: "sirius-addonis.appspot.com",
  messagingSenderId: "410251511172",
  appId: "1:410251511172:web:24982387cfefc5db1ac9d1",
  measurementId: "G-3378J712NN"
};

  export const app = initializeApp(firebaseConfig);
  export const auth = getAuth(app);
  export const firestore = getFirestore(app);
  export const db = getDatabase(app);
  export const storage = getStorage(app);

