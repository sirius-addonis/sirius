export const pages = {
    Home: 'home',
    Addons: 'addons',
    Upload: 'upload',
    About: 'about',
    Profile: 'profile',
    LogIn: 'login',
    LogOut: 'logout',
    Register: 'register',
    AllAddons: 'all-addons'
  };