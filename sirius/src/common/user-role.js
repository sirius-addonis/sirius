export const userRole = {
  GUEST: 0,
  BASIC: 1,
  BLOCKED: 2,
  ADMIN: 3,
};
