import { get, set, ref, query, equalTo, orderByChild, update } from 'firebase/database';
import { db } from '../config/firebase-config';
import { userRole } from '../common/user-role';

export const getUserByHandle = (handle) => {
  return get(ref(db, `users/${handle}`));
};

export const createUserHandle = (handle, uid, email, phone, firstName, lastName, profilePicture) => {

  return set(ref(db, `users/${handle}`), { handle, uid, email, phone, firstName, lastName, profilePicture, comments: {}, likedAddons: {}, role: userRole.BASIC, createdOn: new Date() });
};

export const fromUsersDocument = (snapshot) => {
  const usersDocument = snapshot.val()
  return Object.keys(usersDocument).map(key => {
    const test = usersDocument[key];
    return {
      ...test
    }
  })
}
export const getAllUsers = () => {
  return get(ref(db, 'users'))
  .then(snapshot => {    
    if (!snapshot.exists()) {
      return [];
    }

    return fromUsersDocument(snapshot);
  })
};

export const getUserData = (uid) => {

  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

export const updateUserRole = (handle, role) => {
  return update(ref(db), {
    [`users/${handle}/role`]: role,
  });
};
export const updateFoundIt = (handle) => {
  return update(ref(db) , {
    [`users/${handle}/easterEgg`]: true,
  })
}

export const updateUserProfilePicture = (handle, url) => {
  return update(ref(db), {
    [`users/${handle}/avatarUrl`]: url,
  });
};

export const updateUser = (handle, firstName, lastName, email, image) => {
  const updateProfile = {};
  updateProfile[`/users/${handle}/firstName`]= firstName;
  updateProfile[`/users/${handle}/lastName`]= lastName;
  updateProfile[`/users/${handle}/email`]= email;
  updateProfile[`/users/${handle}/profilePicture`]= image;
  return update(ref(db), updateProfile);
}
export const addUserFeatured = (handle, addonId) => {
  const updateLikes = {};
  updateLikes[`/addons/${addonId}/content/featuredBy/${handle}`] = true;
  updateLikes[`/users/${handle}/featured/${addonId}`] = true;

  return update(ref(db), updateLikes);
}

export const removeUserFeatured = (handle, addonId) => {
  const updateLikes = {};
  updateLikes[`/addons/${addonId}/content/featuredBy/${handle}`] = null;
  updateLikes[`/users/${handle}/featured/${addonId}`] = null;

  return update(ref(db), updateLikes);
}

export const addUserFavorites = (handle, addonId) => {
  const updateLikes = {};
  updateLikes[`/addons/${addonId}/content/addedToFavoritesBy/${handle}`] = true;
  updateLikes[`/users/${handle}/favorites/${addonId}`] = true;

  return update(ref(db), updateLikes);
}

export const removeUserFavorites = (handle, addonId) => {
  const updateLikes = {};
  updateLikes[`/addons/${addonId}/content/addedToFavoritesBy/${handle}`] = null;
  updateLikes[`/users/${handle}/favorites/${addonId}`] = null;

  return update(ref(db), updateLikes);
}


export const fromFavoriteDocument = (snapshot) => {
  const usersDocument = snapshot.val()
  return Object.keys(usersDocument).map(key => {
    return {
       key
    }
  })
}
export const getAllFavorite = (handle) => {
  return get(ref(db, `users/${handle}/favorites`))
  .then(snapshot => {    
    if (!snapshot.exists()) {
      return [];
    }

    return fromFavoriteDocument(snapshot);
  })
};

