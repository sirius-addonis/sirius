import { ref, get, query, equalTo, orderByChild, update, onValue , set} from 'firebase/database';
import { db, storage } from '../config/firebase-config';

import { getStorage, ref as refStorage, getDownloadURL , deleteObject} from "firebase/storage";


export const getDownloadFile = (id) => {
 const storage = getStorage();
  getDownloadURL(refStorage(storage, `files/${id}`))
  .then((url) => {

    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = (event) => {
      const blob = xhr.response;
    };
    xhr.open('GET', url);
    xhr.send();


  })
  .catch((error) => {
    console.log(error.message)
  });
}

export const fromAddonsDocument = snapshot => {
  const addonsDocument = snapshot.val();

  return Object.keys(addonsDocument).map(key => {
    const addon = addonsDocument[key];

    return {
      ...addon,
      id: key,
      createdOn: new Date(addon.createdOn),
      likedBy: addon.likedBy ? Object.keys(addon.likedBy) : [],

    };
  });
};

export const addAddon = async (content, handle , id) => {

  return await set(
    ref(db, `addons/${id}`),
    {
      content,
      author: handle,
      createdOn: Date.now(),
    },
  )
    .then(result => {

      return getAddonById(id);
    });
};

export const getAddonById = async (id) => {

  return await get(ref(db, `addons/${id}`))
    .then(result => {
      if (!result.exists()) {
        throw new Error(`Addon with id ${id} does not exist!`);
      }

      const addon = result.val();
      addon.id = id;
      addon.createdOn = new Date(addon.createdOn);
      if (!addon.likedBy) {
        addon.likedBy = [];
      } else {
        addon.likedBy = Object.keys(addon.likedBy);
      }

      return addon;
    });
};




export const getLikedAddon = async (handle) => {

  return await get(ref(db, `users/${handle}`))
    .then(snapshot => {
      if (!snapshot.val()) {
        throw new Error(`User with handle @${handle} does not exist!`);
      }

      const user = snapshot.val();
      if (!user.likedAddons) return [];

      return Promise.all(Object.keys(user.likedAddons).map(key => {

        return get(ref(db, `addons/${key}`))
          .then(snapshot => {
            const addon = snapshot.val();

            return {
              ...addon,
              createdOn: new Date(addon.createdOn),
              id: key,
              likedBy: addon.likedBy ? Object.keys(addon.likedBy) : [],
            };
          });
      }));
    });
};

export const getAddonByAuthor = async (handle) => {

  return await get(query(ref(db, 'addons'), orderByChild('author'), equalTo(handle)))
    .then(snapshot => {
      if (!snapshot.exists()) return [];

      return fromAddonsDocument(snapshot);
    });
};

export const getLiveAddons = (listen) => {
  return onValue(ref(db, 'addons'), listen);
}

export const getAllAddons = async () => {

  return await get(ref(db, 'addons'))
    .then(snapshot => {
      if (!snapshot.exists()) {
        return [];
      }

      return fromAddonsDocument(snapshot);
    });
};

export const likeAddons = (handle, addonId) => {
  const updateLikes = {};
  updateLikes[`/addons/${addonId}/content/likedBy/${handle}`] = true;
  updateLikes[`/addons/${addonId}/content/dislikedBy/${handle}`] = null;
  updateLikes[`/users/${handle}/likedAddons/${addonId}`] = true;
  updateLikes[`/users/${handle}/dislikedAddons/${addonId}`] = null;

  return update(ref(db), updateLikes);
};

export const counterUpdate = (id , count) => {
  const updateCounter = {};
  updateCounter[`/addons/${id}/content/downloads`] = count;
  return update(ref(db), updateCounter)
}
export const verify = (addonId) => {
  const updateVerify = {};
  updateVerify[`/addons/${addonId}/content/verified`]= true;
  return update(ref(db),updateVerify)
}

export const removeLikeAddon = (handle, addonId) => {
  const updateAddons = {};
  updateAddons[`/addons/${addonId}/content/likedBy/${handle}`] = null;
  updateAddons[`/users/${handle}/likedAddons/${addonId}`] = null;

  return update(ref(db), updateAddons);
};

export const dislikeAddons = (handle, addonId) => {
  const updateAddons = {};
  updateAddons[`/addons/${addonId}/content/dislikedBy/${handle}`] = true;
  updateAddons[`/addons/${addonId}/content/likedBy/${handle}`] = null;
  updateAddons[`/users/${handle}/dislikedAddons/${addonId}`] = true;
  updateAddons[`/users/${handle}/likedAddons/${addonId}`] = null;

  return update(ref(db), updateAddons);
};

export const removeDislikeAddon = (handle, addonId) => {
  const updateAddons = {};
  updateAddons[`/addons/${addonId}/content/dislikedBy/${handle}`] = null;
  updateAddons[`/users/${handle}/dislikedAddons/${addonId}`] = null;

  return update(ref(db), updateAddons);
};


export const deleteAddon = async (id) => {
  const addon = await getAddonById(id);
  const updateLikes = {};

  addon.likedBy.forEach(handle => {
    updateLikes[`/users/${handle}/likedAddons/${id}`] = null;
  });

  
 
  await update(ref(db), updateLikes);

  return update(ref(db), {
    [`/addons/${id}`]: null,
  });
};
export const updateOldVers = (id , info) => {
  return update(ref(db),{
    [`/addons/${id}/content/oldVersions`]: info
  })
}
export const deleteAddonFile = async (id) => {
  const deleteFile = refStorage(storage,`files/${id}`)
  return deleteObject(deleteFile)
  .then(()=>console.log('sUccess'))
  .catch((e)=>console.error(e))
}
export const deleteAddonImage = async (id) =>{
 
  const deleteImage = refStorage(storage, `images/${id}`)
  return   deleteObject(deleteImage)
  .then(()=>console.log('sUccess'))
  .catch((e)=>console.error(e))
}

// export const rateAddons = (handle, addonId) => {
//   const updateRating = {};
//   updateRating[`/addons/${addonId}/content/ratedBy/${handle}`] = true;
//   updateRating[`/users/${handle}/ratedAddons/${addonId}`] = true;

//   return update(ref(db), updateRating);
// };

// export const removeRateAddon = (handle, addonId) => {
//   const updateLikes = {};
//   updateLikes[`/addons/${addonId}/content/likedBy/${handle}`] = null;
//   updateLikes[`/users/${handle}/likedAddons/${addonId}`] = null;

//   return update(ref(db), updateLikes);
// };

export const fromLikedDocument = (snapshot) => {
  const usersDocument = snapshot.val()
  return Object.keys(usersDocument).map(key => {
    const user1 = usersDocument[key];
    return {
      ...user1
    }
  })
}


export const getAllLiked = async (id) => {
  return await get(ref(db, `/addons/${id}/content/likedBy`))
  .then(snapshot => {    
    if (!snapshot.exists()) {
      return [];
    }
    
    return fromLikedDocument(snapshot);
  })
};

export const fromDislikedDocument = (snapshot) => {
  const usersDocument = snapshot.val()
  return Object.keys(usersDocument).map(key => {
    const user1 = usersDocument[key];
    return {
      ...user1
    }
  })
}


export const getAllDisliked = async (id) => {
  return await get(ref(db, `/addons/${id}/content/dislikedBy`))
  .then(snapshot => {    
    if (!snapshot.exists()) {
      return [];
    }

    return fromLikedDocument(snapshot);
  })
};


export const getLiked = async (id, handle) => {
  return await get(ref(db, `/addons/${id}/content/likedBy/${handle}`))
  .then(snapshot => {    
    if (!snapshot.exists()) {
      return [];
    } 
    return fromLikedDocument(snapshot);
  })
};

export const getDisliked = async (id, handle) => {
  return await get(ref(db, `/addons/${id}/content/dislikedBy/${handle}`))
  .then(snapshot => {    
    if (!snapshot.exists()) {
      return [];
    }
    
    return fromDislikedDocument(snapshot);
  })
};



// export const getRatedAddon= async (handle) => {

//   return await get(ref(db, `users/${handle}`))
//     .then(snapshot => {
//       if (!snapshot.val()) {
//         throw new Error(`User with handle @${handle} does not exist!`);
//       }

//       const user = snapshot.val();
//       if (!user.ratedAddon) return [];

//       return Promise.all(Object.keys(user.ratedAddons).map(key => {

//         return get(ref(db, `addons/${key}`))
//           .then(snapshot => {
//             const addon = snapshot.val();

//             return {
//               ...addon,
//               createdOn: new Date(addon.createdOn),
//               id: key,
//               ratedBy: addon.ratedBy ? Object.keys(addon.ratedBy) : [],
//             };
//           });
//       }));
//     });
// };


// export const fromRatedDocument = (snapshot) => {
//   const usersDocument = snapshot.val()
//   return Object.keys(usersDocument).map(key => {
//     const user1 = usersDocument[key];
//     return {
//       ...user1
//     }
//   })
// }


// export const getAllRated = async (id) => {
//   return await get(ref(db, `/addons/${id}/content/ratedBy`))
//   .then(snapshot => {    
//     if (!snapshot.exists()) {
//       return [];
//     }
    
//     return fromRatedDocument(snapshot);
//   })
// };



// export const getRated = async (id, handle) => {
//   return await get(ref(db, `/addons/${id}/content/ratedBy/${handle}`))
//   .then(snapshot => {    
//     if (!snapshot.exists()) {
//       return [];
//     } 
//     return fromRatedDocument(snapshot);
//   })
// };



export const getFavoriteStatus = async (id, handle) => {
  return await get(ref(db, `/addons/${id}/content/addedToFavoritesBy/${handle}`))
  .then(snapshot => {
    if (snapshot.exists()) {
      return true;
    } else {
      return false;
    }

  })
};
export const getFeaturedStatus = async (id,handle) => {
  return await get(ref(db, `addons/${id}/content/featuredBy/${handle}`))
  .then(snapshot => {
    if(snapshot.exists()){
      return true;
    } else {
      return false
    }
  })
}

export const checkIsLikedByHandle = async (addonId, handle) => {
  return await get(ref(db, `/addons/${addonId}/content/likedBy/${handle}`))
  .then(snapshot => {
    if (snapshot.exists()) {
      return true;
    } else {
      return false;
    }
  })
};

export const checkIsDislikedByHandle = async (addonId, handle) => {
  return await get(ref(db, `/addons/${addonId}/content/dislikedBy/${handle}`))
  .then(snapshot => {
    if (snapshot.exists()) {
      return true;
    } else {
      return false;
    }
  })
};


export const fromSortedDocument = (snapshot) => {
  const usersDocument = snapshot.val()
  return Object.keys(usersDocument).map(key => {
    const user1 = usersDocument[key];
    return {
      ...user1
    }
  })
}

export const setRatingDataFunction = (addonId, rating) => {
   const updateRating = {};
   updateRating[`/addons/${addonId}/content/rating`] = rating;

   return update(ref(db), updateRating);
};


export const getRating =  (addonId) => {
  return  get(ref(db, `/addons/${addonId}/content/rating`))
  .then(snapshot => {
    if (snapshot.exists()) {
      return snapshot.val();
    } else {
      return 0;
    }

  })
};



