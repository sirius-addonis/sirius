import { ref, push, get, query, equalTo, orderByChild, update, onValue , set, orderByValue} from 'firebase/database';
import { db } from '../config/firebase-config';
export const fromCommentsDocument = async snapshot => {
    const commentIds = Object.keys(snapshot.val());
  
    return await Promise.all(commentIds.map(id => {
      return getCommentById(id);
    }));
  };
  
  export const addComment = async (comment, handle, idOfComment, idOfAddon, profilePictureURL) => {

    return await set(
      ref(db, `comments/${idOfComment}`),
      {
        content:{
          textComment: comment,
          addonId: idOfAddon,
          profilePictureURL: profilePictureURL,
        }, 
        author: handle,
        createdOn: Date.now(),
      },
    )
      .then(result => {
        return getCommentById(idOfComment);
      });
  };
  
  export const getCommentById = async (id) => {

    return await get(ref(db, `comments/${id}`))
      .then(result => {
        if (!result.exists()) {
          throw new Error(`Comment with id ${id} does not exist!`);
        }
  
        const comment = result.val();
        comment.id = id;
        comment.createdOn = new Date(comment.createdOn);
        if (!comment.likedBy) {
          comment.likedBy = [];
        } else {
          comment.likedBy = Object.keys(comment.likedBy);
        }
  
        return comment;
      });
  };

  export const getCommentsByAddon = async (id) => {
    return await get(ref(db, `addons/${id}`))
    .then (result => {
        if(!result.exists()) {
            throw new Error(`Addon with id ${id} does not exist!`);
        }

        const comment = result.val();
        comment.id = id;
        comment.createdOn = new Date(comment.createdOn);
        if (!comment.likedBy) {
          comment.likedBy = [];
        } else {
          comment.likedBy = Object.keys(comment.likedBy);
        }
  
        return comment;
    })
  }
  export const getCommentsByAuthor = async (handle) => {

    return await get(query(ref(db, 'comments'), orderByChild('author'), equalTo(handle)))
      .then(snapshot => {
        if (!snapshot.exists()) return [];
  
        return fromCommentsDocument(snapshot);
      });
  };

  export const commentAddons = (handle, commentId, addonId) => {
    const updateComments = {};
    updateComments[`/addons/${addonId}/content/commentedBy/${handle}`] = true;
    updateComments[`/addons/${addonId}/content/commentsIds/${commentId}`] = true;
    updateComments[`/users/${handle}/commentedAddons/${addonId}`] = true;

    return update(ref(db), updateComments);
  };


  export const getAllComments = async (addonId) => {
    return await get(ref(db, `addons/${addonId}/content/commentsIds`))
      .then(snapshot => {
        if (!snapshot.exists()) {
          return [];
        }

        return fromCommentsDocument(snapshot);
      });
  };


  export const deleteComment = async (commentId, addonId, handle) => {
  
    return update(ref(db), {
      [`/comments/${commentId}`]: null,
      [`/addons/${addonId}/content/commentsIds/${commentId}`]: null,
      
    });
  };

