
# Sirius - Addonis Project

Sirius is our final projects as students at Telerik Academy.
It is a Addons Registry single-page web application and we chose to implement it with a space theme.

The users must register and log in to have access to all features:
- view all addons sorted by rating, date, featured and also view their favorite addons
- upload, edit and delete their own addons
- search addon by title, by tag, by IDE
- comment addons
- upvote/downvote the addons that they like or dislike
- each user can be promoted to Admin by another Admin and can search and view details for all users' profiles, have -- - rights to delete all addons and comments, block / unblock users and make other Admins users.
- change light and dark mode

The project is made on Node.js with JavaScript and React and for authentication and data storage we used Firebase.

GitLab link: https://gitlab.com/sirius-addonis

This project was created with `React and Bootstrap`.


In this project directory, you can run: `npm install`, then `npm start`.



Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.




## Logo
![logo](./src/images/logo.jpg "Logo")

## Banner
![Banner](./src/images/banner.jpg "Banner")

# Final Project Screenshots

## Home page
![Home](./src/images/readme-images/Screenshot_10.jpg "Home")

### Home page - Top Addons
![Top](./src/images/readme-images/Screenshot_11.jpg "Top")

### Home page - Featured Addons
![Featured](./src/images/readme-images/Screenshot_12.jpg "Featured")

### Home page - New Addons
![New](./src/images/readme-images/Screenshot_13.jpg "New")

## White and Dark mode
![white and dark](./src/images/readme-images/Screenshot_14.jpg "White and Dark")

## All Addons, Feaured, Favorite, Most Popular, New Addons and To be Verified
![All addons](./src/images/readme-images/Screenshot_15.jpg "All addons")

## Profile
![Profile](./src/images/readme-images/Screenshot_16.jpg "Profile")

## Profile Edit
![Profile Edit](./src/images/readme-images/Screenshot_17.jpg "Profile Edit")

## View Details
![View Details](./src/images/readme-images/Screenshot_18.jpg "View Details")

## Comments
![Comments](./src/images/readme-images/Screenshot_19.jpg "Comments")

## Page not found
![404](./src/images/readme-images/Screenshot_20.jpg "404")




# Mockups history

## Home Page 
![home view](./src/images/readme-images/Screenshot_1.jpg "home view")

## All Addons
![All Addonis](./src/images/readme-images/Screenshot_2.jpg "All Addonis")

## View Details
![View Details](./src/images/readme-images/Screenshot_3.jpg "View Details")

## Upload
![Upload](./src/images/readme-images/Screenshot_4.jpg "Upload")

## Easter Egg
![Upload](./src/images/readme-images/Screenshot_5.jpg "Upload")

## Admin
![Upload](./src/images/readme-images/Screenshot_6.jpg "Upload")

